﻿namespace QSD_DevForce.Vsix
{
    using System;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio;
    using Microsoft.VisualStudio.Shell;
    using Microsoft.VisualStudio.Shell.Interop;

    /// <summary>
    /// This class implements the tool window exposed by this package and hosts a user control.
    /// </summary>
    /// <remarks>
    /// In Visual Studio tool windows are composed of a frame (implemented by the shell) and a pane,
    /// usually implemented by the package implementer.
    /// <para>
    /// This class derives from the ToolWindowPane class provided from the MPF in order to use its
    /// implementation of the IVsUIElementPane interface.
    /// </para>
    /// </remarks>
    [Guid("63764fb5-f5f2-4315-995a-a371e0019a9b")]
    public class QSD_DevForce_Window : ToolWindowPane
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QSD_DevForce_Window"/> class.
        /// </summary>

        public QSD_DevForce_Window() : base(null)
        {
            this.Caption = "QSD_DevForce";

            // This is the user control hosted by the tool window; Note that, even if this class implements IDisposable,
            // we are not calling Dispose on this object. This is because ToolWindowPane calls Dispose on
            // the object returned by the Content property.
            this.Content = new QSD_DevForce_Window_Control();
        }

    }
}
