﻿namespace QSD_DevForce.Vsix
{
    using EnvDTE;
    using EnvDTE80;
    using QSD_DevForce.Analyzer;
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;

    /// <summary>
    /// Interaction logic for ToolWindow1Control.
    /// </summary>
    public partial class QSD_DevForce_Window_Control : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolWindow1Control"/> class.
        /// </summary>

        public ObservableCollection<Pattern> Collection { get; set; }
        private object _storageLock = new Object();

        public QSD_DevForce_Window_Control()
        {
            Collection = PatternCollection.getInstance();
            BindingOperations.EnableCollectionSynchronization(Collection, _storageLock);
            this.InitializeComponent();
            this.DataContext = this;
        }

        private void OpenFile_OnClick(object sender, RoutedEventArgs e)
        {
            var button = (Button)e.Source;
            var uri = (Uri)button.Content;
            var dte = QSD_DevForce_Window_Package.GetGlobalService(typeof(DTE)) as DTE2;
            Microsoft.VisualStudio.Shell.ThreadHelper.ThrowIfNotOnUIThread();
            dte.ItemOperations.OpenFile(uri.AbsoluteUri);
        }
    }
}