﻿using Microsoft.CodeAnalysis;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.CodeAnalysis.Diagnostics;
using QSD_DevForce.Analyzer.Rule;
using System.Threading.Tasks;
using VerifyCS = QSD_DevForce.Test.CSharpAnalyzerVerifier<QSD_DevForce.Analyzer.SingletonAnalyzer>;
using QSD_DevForce.Analyzer;

namespace QSD_DevForce.Test
{
    [TestClass]
    public class SingletonAnalyzerTests
    {
        private const string singleton = @"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace AnalyzerTestSolution
            {
                class Logger
                {
                    private static Logger _instance;

                    private Logger()
                    {
                    }

                    public static Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }";
        private const string singletonWithoutStaticField = @"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace AnalyzerTestSolution
            {
                class Logger
                {
                    private Logger _instance;

                    private Logger()
                    {
                    }

                    public Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }";
        private const string constructorNotPrivate = @"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace AnalyzerTestSolution
            {
                class Logger
                {
                    private static Logger _instance;

                    public Logger()
                    {
                    }

                    public static Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }";
        private const string fieldNotStatic = @"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace AnalyzerTestSolution
            {
                class Logger
                {
                    private Logger _instance;

                    private Logger()
                    {
                    }

                    public Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }";
        private const string fieldNotPrivate = @"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace AnalyzerTestSolution
            {
                class Logger
                {
                    public static Logger _instance;

                    private Logger()
                    {
                    }

                    public static Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }";
        private const string noStaticMethodReturnOwnClass = @"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace AnalyzerTestSolution
            {
                class Logger
                {
                    private static Logger _instance;

                    private Logger()
                    {
                    }

                }
            }";

        [DataTestMethod]
        [
            DataRow(""),
            DataRow(singletonWithoutStaticField),
            DataRow(constructorNotPrivate),
            DataRow(fieldNotStatic),
            DataRow(fieldNotPrivate),
            DataRow(noStaticMethodReturnOwnClass),
        ]
        public async Task SingletonNotDetected(string testData)
        {
            await VerifyCS.VerifyAnalyzerAsync(testData);
        }

        [DataTestMethod]
        [DataRow(singleton)]
        public async Task SingletonDetected(string testData)
        {
            var rule = new DiagnosticDescriptor("SingletonAnalyzer", "Singleton class", "This is a singleton class", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            var expected = VerifyCS.Diagnostic(rule).WithSpan(6, 13, 28, 14);

            await VerifyCS.VerifyAnalyzerAsync(testData, expected);
        }

        [DataTestMethod]
        [
            DataRow(""),
            DataRow(constructorNotPrivate),
        ]
        public async Task PrivateConstructorNotDetected(string testData)
        {
            await CSharpAnalyzerVerifier<PrivateConstructorTestAnalyzer>.VerifyAnalyzerAsync(testData);
        }

        [DataTestMethod]
        [DataRow(singleton)]
        public async Task PrivateConstructorDetected(string testData)
        {
            var rule = new DiagnosticDescriptor("PrivateConstructorAnalyzer", "Private Constructor", "This is a private Constructor", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            var expected = VerifyCS.Diagnostic(rule).WithSpan(6, 13, 28, 14);
            await CSharpAnalyzerVerifier<PrivateConstructorTestAnalyzer>.VerifyAnalyzerAsync(testData, expected);
        }

        [DataTestMethod]
        [
            DataRow(""),
            DataRow(fieldNotPrivate),
            DataRow(fieldNotStatic),
        ]
        public async Task PrivateStaticFieldNotDetected(string testData)
        {
            await CSharpAnalyzerVerifier<PrivateStaticFieldTestAnalyzer>.VerifyAnalyzerAsync(testData);
        }

        [DataTestMethod]
        [DataRow(singleton)]
        public async Task PrivateStaticFieldDetected(string testData)
        {
            var rule = new DiagnosticDescriptor("PrivateStaticFieldAnalyzer", "Private Static Field", "This is a private static field", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            var expected = VerifyCS.Diagnostic(rule).WithSpan(6, 13, 28, 14);
            await CSharpAnalyzerVerifier<PrivateStaticFieldTestAnalyzer>.VerifyAnalyzerAsync(testData, expected);
        }

        [DataTestMethod]
        [
            DataRow(""),
            DataRow(noStaticMethodReturnOwnClass)
        ]
        public async Task StaticMethodReturnOwnClassNotDetected(string testData)
        {
            await CSharpAnalyzerVerifier<StaticMethodReturnOwnClassTestAnalyzer>.VerifyAnalyzerAsync(testData);
        }

        [DataTestMethod]
        [DataRow(singleton)]
        public async Task StaticMethodReturnOwnClassDetected(string testData)
        {
            var rule = new DiagnosticDescriptor("StaticMethodReturnOwnClassAnalyzer", "Static method return own class", "This is a static method that returns is own class", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            var expected = VerifyCS.Diagnostic(rule).WithSpan(6, 13, 28, 14);
            await CSharpAnalyzerVerifier<StaticMethodReturnOwnClassTestAnalyzer>.VerifyAnalyzerAsync(testData, expected);
        }

    }


    /*
     * Analyzer classes to make seperate analyzers for each rule
     */

    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class PrivateConstructorTestAnalyzer : Analyzer.Analyzer
    {
        public override DiagnosticDescriptor Title
        {
            get
            {
                return new DiagnosticDescriptor("PrivateConstructorAnalyzer", "Private Constructor", "This is a private Constructor", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            }
        }

        public override string PatternName { get; set; }
        public override string PatternPart { get; set; }

        public PrivateConstructorTestAnalyzer()
        {
            rule = new HasPrivateConstructor(new Rule());
        }
    }

    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class PrivateStaticFieldTestAnalyzer : Analyzer.Analyzer
    {
        public override DiagnosticDescriptor Title
        {
            get
            {
                return new DiagnosticDescriptor("PrivateStaticFieldAnalyzer", "Private Static Field", "This is a private static field", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            }
        }

        public override string PatternName { get; set; }
        public override string PatternPart { get; set; }

        public PrivateStaticFieldTestAnalyzer()
        {
            rule = new HasPrivateStaticField(new Rule());
        }
    }

    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class StaticMethodReturnOwnClassTestAnalyzer : Analyzer.Analyzer
    {
        public override DiagnosticDescriptor Title
        {
            get
            {
                return new DiagnosticDescriptor("StaticMethodReturnOwnClassAnalyzer", "Static method return own class", "This is a static method that returns is own class", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            }
        }

        public override string PatternName { get; set; }
        public override string PatternPart { get; set; }

        public StaticMethodReturnOwnClassTestAnalyzer()
        {
            rule = new HasStaticMethodReturnOwnClass(new Rule());
        }
    }
}
