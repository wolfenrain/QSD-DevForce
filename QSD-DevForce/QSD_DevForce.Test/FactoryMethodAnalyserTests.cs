﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using QSD_DevForce.Analyzer.Rule;
using VerifyCS = QSD_DevForce.Test.CSharpAnalyzerVerifier<QSD_DevForce.Analyzer.FactoryMethodAnalyser>;

namespace QSD_DevForce.Test
{
    [TestClass]
    public class FactoryMethodAnalyserTests
    {
        private const string interfaceCode = @"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace AnalyzerTestSolution.Factory
            {
                interface CreditCard
                {
                    public abstract string CardType { get; }
                    public abstract int CreditLimit { get; set; }
                    public abstract int AnnualCharge { get; set; }
                }
            }            
            ";

        private const string noInterfaceCode = @"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace AnalyzerTestSolution.Factory
            {
                class CreditCard
                {
                    public string CardType { get; }
                    public int CreditLimit { get; set; }
                    public int AnnualCharge { get; set; }
                }
            }            
            ";

        [DataTestMethod]
        [
            DataRow(noInterfaceCode)
        ]
        public async Task ClassIsInterfaceNotDetected(string testData)
        {
            await CSharpAnalyzerVerifier<IsInterfaceAnalyzer>.VerifyAnalyzerAsync(testData);
        }

        [DataTestMethod]
        [DataRow(interfaceCode)]
        public async Task ClassIsInterfaceDetected(string testData)
        {
            var rule = new DiagnosticDescriptor("IsInterfaceAnalyzer", "Code contains interface", "This is a piece of code that contains an interface", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            var expected = VerifyCS.Diagnostic(rule).WithSpan(6, 13, 14, 14);
            await CSharpAnalyzerVerifier<IsInterfaceAnalyzer>.VerifyAnalyzerAsync(testData, expected);
        }

        [DiagnosticAnalyzer(LanguageNames.CSharp)]
        public class IsInterfaceAnalyzer : Analyzer.Analyzer
        {
            public override DiagnosticDescriptor Title
            {
                get
                {
                    return new DiagnosticDescriptor("IsInterfaceAnalyzer", "Code contains interface", "This is a piece of code that contains an interface", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
                }
            }

            public override string PatternName { get; set; }
            public override string PatternPart { get; set; }

            public IsInterfaceAnalyzer()
            {
                rule = new IsInterface(new Rule());
            }
        }
    }
}
