﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using VerifyCS = QSD_DevForce.Test.CSharpAnalyzerVerifier<QSD_DevForce.Analyzer.ObserverAnalyzer>;


namespace QSD_DevForce.Test
{
    [TestClass]
    public class ObserverAnalyzerTests
    {
        [TestMethod]
        public async Task WhenObserverPatternIsNotDetected()
        {
            // Changed ISubscriber to string and commented out the 'subscriber.Update() function, because there is not subscriber object.
            var observerPattern = @"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace AnalyzerTestSolution.Observer
            {
                class Publisher
                {
                    private List<string> subscribers;                    

                    public void Subscribe(string subscriber)
                    {
                        subscribers.Add(subscriber);
                    }

                    public void UnSubscribe(string subscriber)
                    {
                        subscribers.Remove(subscriber);
                    }

                    public void NotifySubscribers()
                    {
                        foreach(var subscriber in subscribers)
                        {
                            //subscriber.Update(""Updated message.""); 
                        }
                    }
                }
            }
            ";
            await VerifyCS.VerifyAnalyzerAsync(observerPattern);
        }



    }
}
