﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.CodeAnalysis.CSharp;
using System.Linq;
using QSD_DevForce.Syntax;
using System.Collections.Generic;

namespace QSD_DevForce.Test
{
    [TestClass]
    public class SyntaxManagerTests
    {

        private readonly SyntaxTree testClass = CSharpSyntaxTree.ParseText(@"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace SyntaxManagerTests
            {
                class Logger
                {
                    private static Logger _instance;

                    public Logger()
                    {
                    }

                    public static Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }");

        private readonly SyntaxTree testClassPrivateConstructor = CSharpSyntaxTree.ParseText(@"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace SyntaxManagerTests
            {
                class Logger
                {
                    private static Logger _instance;

                    private Logger()
                    {
                    }

                    public static Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }");


        private readonly SyntaxTree testClassNoConstructor = CSharpSyntaxTree.ParseText(@"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace SyntaxManagerTests
            {
                class Logger
                {
                    private static Logger _instance;

                    public static Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }");

        private readonly SyntaxTree testClassNoField = CSharpSyntaxTree.ParseText(@"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace SyntaxManagerTests
            {
                class Logger : ILogger
                {
                    
                    public static Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }");

        private readonly SyntaxTree testClassNoMethod = CSharpSyntaxTree.ParseText(@"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace SyntaxManagerTests
            {
                class Logger
                {
                    private static Logger _instance;

                }
            }");

        private readonly SyntaxTree testClassNoFields = CSharpSyntaxTree.ParseText(@"
            using System;
            using System.Collections.Generic;
            using System.Text;

            namespace SyntaxManagerTests
            {
                class Logger
                {
                    public Logger()
                    {
                    }

                    public static Logger GetLogger()
                    {
                        if (_instance == null)
                        {
                            return _instance = new Logger();
                        }
                        else
                        {
                            return _instance;
                        }
                    }
                }
            }");

        [TestMethod]
        // This unit test verifies if the method returns a constructor.
        public void GetConstructorSyntax()
        {
            // Arrange
            var test = testClass.GetRoot().DescendantNodes().OfType<ConstructorDeclarationSyntax>().First();
            var nameSpace = testClass.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            var expected = test;

            // Act
            var result = SyntaxManager.GetConstructorSyntax(nameSpace);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies if there is no constructor in a class.
        public void GetNoConstructorSyntax()
        {
            // Arrange
            var nameSpace = testClassNoConstructor.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            string expected = null;

            // Act
            var result = SyntaxManager.GetConstructorSyntax(nameSpace);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies if there is a constructor that matches the Singleton design pattern.
        public void GetSingleTonFieldSyntax()
        {
            // Arrange
            var nameSpace = testClassPrivateConstructor.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            var constructorDeclaration = testClassPrivateConstructor.GetRoot().DescendantNodes().OfType<FieldDeclarationSyntax>().First();
            var expected = constructorDeclaration;

            // Act
            var result = SyntaxManager.GetSingletonFieldSyntax(nameSpace);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies if there is no constructor that matches the Singleton design pattern.
        public void GetNoSingleTonFieldSyntax()
        {
            // Arrange
            var nameSpace = testClassNoField.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            string expected = null;

            // Act
            var result = SyntaxManager.GetSingletonFieldSyntax(nameSpace);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies if the method returns fields.
        public void GetFieldSyntax()
        {
            // Arrange
            var test = testClass.GetRoot().DescendantNodes().OfType<FieldDeclarationSyntax>().First();
            var nameSpace = testClass.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            List<FieldDeclarationSyntax> expected = new List<FieldDeclarationSyntax>
            {
                test
            };

            // Act
            var result = SyntaxManager.GetFieldSyntax(nameSpace);

            // Assert
            CollectionAssert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies the method returns a method that matches the Singleton design pattern.
        public void GetSingletonMethodSyntax()
        {
            // Arrange
            var test = testClass.GetRoot().DescendantNodes().OfType<MethodDeclarationSyntax>().First();
            var nameSpace = testClass.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            var expected = test;

            // Act
            var result = SyntaxManager.GetSingletonMethodSyntax(nameSpace);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies if there is no method found that matches the Singleton design pattern.
        public void GetNoSingletonMethodSyntax()
        {
            // Arrange
            var nameSpace = testClassNoMethod.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            string expected = null;

            // Act
            var result = SyntaxManager.GetSingletonMethodSyntax(nameSpace);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies the GetMethodSyntax to see if it finds a method.
        public void GetMethodSyntax()
        {
            // Arrange
            var test = testClass.GetRoot().DescendantNodes().OfType<MethodDeclarationSyntax>().First();
            var nameSpace = testClass.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            List<MethodDeclarationSyntax> expected = new List<MethodDeclarationSyntax>
            {
                test
            };

            // Act
            var result = SyntaxManager.GetMethodSyntax(nameSpace);

            // Assert
            CollectionAssert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies the GetInterface method to see if it returns the implementation of an interface.
        public void GetInterface()
        {
            // Arrange
            var nameSpace = testClassNoField.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            List<string> expected = new List<string>
            {
                "ILogger"
            };

            // Act
            var result = SyntaxManager.GetInterface(nameSpace);

            // Assert
            CollectionAssert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies the GetInterface method to see if it returns no implementation of an interface and instead returns null.
        public void GetNoInterface()
        {
            // Arrange
            var nameSpace = testClass.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            List<string> expected = null;

            // Act
            var result = SyntaxManager.GetInterface(nameSpace);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies the GetClassName method to see if it returns class names.
        public void GetClassName()
        {
            // Arrange
            var nameSpace = testClass.GetRoot().DescendantNodes().OfType<NamespaceDeclarationSyntax>().First();
            var expected = "Logger";

            // Act
            var result = SyntaxManager.GetClassName(nameSpace);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies the GetFieldIdentifiers method to see if it returns field identifiers.
        public void GetFieldIdentifiers()
        {
            // Arrange
            var test = testClass.GetRoot().DescendantNodes().OfType<FieldDeclarationSyntax>().First();
            List<FieldDeclarationSyntax> resultList = new List<FieldDeclarationSyntax>
            {
                test
            };

            // Act
            var result = SyntaxManager.GetFieldIdentifiers(resultList);
            List<string> expected = new List<string>
            {
                "Logger"
            };

            // Assert
            CollectionAssert.AreEqual(expected, result);
        }

        [TestMethod]
        // This unit test verifies the GetFieldIdentifiers method to see if it returns no field identifiers and instead returns null.
        public void GetNoFieldIdentifiers()
        {
            // Arrange
            FieldDeclarationSyntax test = testClassNoFields.GetRoot().DescendantNodes().OfType<FieldDeclarationSyntax>().FirstOrDefault();
            List<FieldDeclarationSyntax> resultList = new List<FieldDeclarationSyntax>
            {
                test
            };
            List<string> expected = null;

            // Act
            var result = SyntaxManager.GetFieldIdentifiers(resultList);

            // Assert
            CollectionAssert.AreEqual(expected, result);
        }

    }

}
