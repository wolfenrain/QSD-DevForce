﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using QSD_DevForce.Analyzer.Rule;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using System;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace QSD_DevForce.Analyzer
{
    public abstract class Analyzer : DiagnosticAnalyzer
    {
        public IRule rule;
        public ObservableCollection<Pattern> Collection { get; set; } = PatternCollection.getInstance();
        private readonly object _storageLock = new object();
        public abstract DiagnosticDescriptor Title { get; }
        public abstract string PatternName { get; set; }
        public abstract string PatternPart { get; set; }
        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(Title); } }
        public CSharpSyntaxNode Analyze(SyntaxNodeAnalysisContext context)
        {
            try
            {
                if (rule == null)
                {
                    return null;
                }

                NamespaceDeclarationSyntax result;

                try
                {
                    result = rule.Analyze((NamespaceDeclarationSyntax)context.Node);
                }
                catch
                {
                    result = null;
                }

                if (result != null)
                {

                    context.ReportDiagnostic(Diagnostic.Create(Title, result.GetLocation()));
                    lock (_storageLock)
                    {
                        foreach (var pattern in rule.Patterns)
                        {
                            pattern.Title = rule.PatternGroupIdentifier ?? PatternName;
                            pattern.Part = PatternPart;

                            if (!Collection.Any(p => p.Title == pattern.Title && p.Part == pattern.Part && p.TargetType == pattern.TargetType && p.Location == pattern.Location))
                            {
                                Collection.Add(pattern);
                            }
                        }
                    }
                }
                else
                {
                    lock (_storageLock)
                    {
                        foreach (var foundPattern in rule.Patterns)
                        {
                            var name = rule.PatternGroupIdentifier ?? PatternName;

                            foreach (var pattern in Collection.Where(t => t.Title == name).ToList())
                            {
                                Collection.Remove(pattern);
                            }
                        }
                        rule.Patterns.Clear();
                    }
                }
            }
            catch
            {
                return null;
            }

            return null;
        }

        public override void Initialize(AnalysisContext context)
        {
            context.EnableConcurrentExecution();
            context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.Analyze | GeneratedCodeAnalysisFlags.ReportDiagnostics);
            context.RegisterSemanticModelAction(SemanticModelAction);
            context.RegisterSyntaxNodeAction(ContextAnalyzer, SyntaxKind.NamespaceDeclaration);
        }

        private void SemanticModelAction(SemanticModelAnalysisContext obj)
        {
            try
            {
                if (rule == null)
                {
                    return;
                }

                rule.StoredSemanticModel = obj.SemanticModel;
            }
            catch
            {
                return;
            }
        }

        public void ContextAnalyzer(SyntaxNodeAnalysisContext _this)
        {
            Analyze(_this);
        }

    }
}
