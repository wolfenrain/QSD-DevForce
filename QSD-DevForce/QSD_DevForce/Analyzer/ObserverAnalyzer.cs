﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using QSD_DevForce.Analyzer.Rule;

namespace QSD_DevForce.Analyzer
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class ObserverAnalyzer : Analyzer
    {
        public override DiagnosticDescriptor Title
        {
            get
            {
                return new DiagnosticDescriptor("ObserverAnalyzer", "Observer pattern", "This is the observer pattern", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            }
        }

        public override string PatternName { get; set; }
        public override string PatternPart { get; set; }

        public ObserverAnalyzer()
        {
            rule = new HasFieldWithSubscribers(new Rule.Rule(), "Field with subscriber");
            rule = new HasMethodWithSubscribers(rule, "Method with subscriber");
            PatternName = "Observer pattern";
            PatternPart = "Observer";
        }
    }
}
