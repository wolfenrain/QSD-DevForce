﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using QSD_DevForce.Analyzer.Rule;
using System;

namespace QSD_DevForce.Analyzer
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class SingletonAnalyzer : Analyzer
    {
        public override DiagnosticDescriptor Title
        {
            get
            {
                return new DiagnosticDescriptor("SingletonAnalyzer", "Singleton class", "This is a singleton class", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            }
        }

        public override string PatternName { get; set; }
        public override string PatternPart { get; set; }


        public SingletonAnalyzer()
        {
            //Rule to find a private constructor
            rule = new HasPrivateConstructor(new Rule.Rule(), "Private Constructor");

            //rule to find the private static singleton instance field
            rule = new HasPrivateStaticField(rule, "Private Static Field");

            //rule to find the method that returns the singleton instance
            rule = new HasStaticMethodReturnOwnClass(rule, "Static Method Return Class");

            //Information to display in the plug-in window
            PatternName = "Singleton";
            PatternPart = "Singleton class";
        }

    }
}
