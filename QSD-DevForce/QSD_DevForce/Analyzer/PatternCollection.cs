﻿using System;
using System.Collections.ObjectModel;
using System.Text;

namespace QSD_DevForce.Analyzer
{
    public class PatternCollection : ObservableCollection<Pattern>
    {
        private PatternCollection() : base() { }
        private static PatternCollection _instance;

        public static PatternCollection getInstance()
        {
            if (_instance != null)
            {
                return _instance;
            }
            else
            {
                _instance = new PatternCollection();
                return _instance;
            }
        }
    }
}
