using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using QSD_DevForce.Analyzer.Rule;
using System;

namespace QSD_DevForce.Analyzer
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class FactoryMethodAnalyser : Analyzer
    {
        public override DiagnosticDescriptor Title
        {
            get
            {
                return new DiagnosticDescriptor("FactoryMethodAnalyser", "FactoryMethod class", "This is a FactoryMethod class", "Category", DiagnosticSeverity.Info, isEnabledByDefault: true, description: "Description");
            }
        }

        public override string PatternName { get; set; }
        public override string PatternPart { get; set; }

        public FactoryMethodAnalyser()
        {
            PatternName = "Factory Method";
            PatternPart = "Factory Method Pattern";

            /// Define rules that make up the analyser.
            /// 
            // Find classes that implement creator class. Matches = concrete creators
            rule = new FindClassImplementation(new Rule.Rule(), "ConcreteCreator");

            // Add Concrete products
            rule = new IsConcreteProduct(rule, "ConcreteProduct");

            // Check if abstract class returns an object of type interface. If true = Creator class found;
            rule = new HasMethodReturnsType(rule, "Creator");

            // Check found class is abstract.
            rule = new FindAbstractClassInClasslist(rule);

            // Find classes that implement interface.
            rule = new FindImplementation(rule);

            // To Detect implements interface.
            rule = new IsInterface(rule, "Product");
        }
    }
}