﻿using System;
using System.ComponentModel;
using System.Text;

namespace QSD_DevForce.Analyzer
{
    public class Pattern : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public string Title { get; set; }
        public string TargetType { get; set; }
        public string Part { get; set; }
        public Uri Location { get; set; }

    }
}
