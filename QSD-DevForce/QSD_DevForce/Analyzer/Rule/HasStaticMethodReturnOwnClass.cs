﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;

namespace QSD_DevForce.Analyzer.Rule
{
    public class HasStaticMethodReturnOwnClass : RuleDecorator
    {
        bool isPublic = false;
        bool isStatic = false;

        public HasStaticMethodReturnOwnClass(IRule rule) : base(rule) { }
        public HasStaticMethodReturnOwnClass(IRule rule, string targetType) : base(rule, targetType) { }

        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax node)
        {
            //get the singleton method
            MethodDeclarationSyntax methodNode = SyntaxManager.GetSingletonMethodSyntax(node);

            //Checks if a function is static and public. If both are true it passes the node to the next rule.
            if (methodNode != null)
            {
                foreach (var item in methodNode.Modifiers)
                {
                    if (item.ValueText == "public")
                    {
                        isPublic = true;
                    }

                    if (item.ValueText == "static")
                    {
                        isStatic = true;
                    }
                }

                if (isPublic && isStatic)
                {
                    isPublic = false;
                    isStatic = false;
                    addPattern(node);
                    return PartOf.Analyze(node);
                }
            }
            throw new RuleException("No static method found that retuns the singleton instance");
        }
    }
}
