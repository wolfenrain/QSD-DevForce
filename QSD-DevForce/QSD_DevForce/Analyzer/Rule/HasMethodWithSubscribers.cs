﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;
using System;
using System.Collections.Generic;

namespace QSD_DevForce.Analyzer.Rule
{
    public class HasMethodWithSubscribers : RuleDecorator
    {
        public HasMethodWithSubscribers(IRule rule) : base(rule) { }
        public HasMethodWithSubscribers(IRule rule, string targetType) : base(rule, targetType) { }

        /// <summary>
        /// Analyzes the methods in a given NamespaceDeclarationSyntax and identifies the subscribers.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax context)
        {
            // Gets the classname of the current class that is being analyzed. 
            this.PatternGroupIdentifier = SyntaxManager.GetClassName(context);

            List<FieldDeclarationSyntax> fieldList = SyntaxManager.GetFieldSyntax(context);
            List<string> fieldIdentifiers = SyntaxManager.GetFieldIdentifiers(fieldList);

            //loops through the list of methods to see if there are methods that utilize the field identifier. 
            List<MethodDeclarationSyntax> methodList = SyntaxManager.GetMethodSyntax(context);
            foreach (var item in methodList)
            {
                var parameterList = item.ParameterList.Parameters;
                foreach (var parameter in parameterList)
                {
                    if (IsFieldIdentifierInterface(fieldIdentifiers))
                    {
                        foreach (var identifier in fieldIdentifiers)
                        {
                            if (parameter.GetText().ToString().Contains(identifier))
                            {
                                addPattern(context);
                                return PartOf.Analyze(context);
                            }
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// The method checks if given string is an interface within the solution.
        /// </summary>
        /// <param name="identifiers"></param>
        /// <returns></returns>
        public bool IsFieldIdentifierInterface(List<string> identifiers)
        {
            List<string> interfaces = new List<string>();
            bool isInterface = false;

            try
            {
                var model = this.StoredSemanticModel.Compilation.SyntaxTrees;

                foreach (var file in model)
                {
                    // Get a SyntaxNode of the file to browse through.
                    var tree = (CompilationUnitSyntax)file.GetRoot();

                    // Create an iterable of the tree
                    var children = tree.DescendantNodes();

                    // Iterate through the file's nodes to find a NamespaceDeclarationSyntax
                    foreach (var child in children)
                    {
                        if (child.GetType() == typeof(NamespaceDeclarationSyntax))
                        {
                            // Een namespacedeclarationsyntax gevonden :o
                            var parsedChild = (NamespaceDeclarationSyntax)child;

                            // Implemented interfaces
                            interfaces = SyntaxManager.GetInterface(parsedChild);
                        }
                    }
                }
            }
            catch (Exception e)
            {

                var error = e.Message;
            }
            try
            {

            }
            catch (Exception)
            {

                throw;
            }

            // Checks if the identifier is an interface and sets 'isInterface' to true if it is an interface.
            try
            {
                foreach (var identifier in identifiers)
                {
                    foreach (var item in interfaces)
                    {
                        if (item == identifier)
                        {
                            isInterface = true;
                        }
                    }
                }
            }
            catch
            {
                throw new RuleException("Interfaces not found.");
            }

            return isInterface;
        }
    }
}
