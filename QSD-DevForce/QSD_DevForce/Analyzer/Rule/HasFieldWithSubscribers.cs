﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;
using System;
using System.Collections;
using System.Collections.Generic;

namespace QSD_DevForce.Analyzer.Rule
{
    public class HasFieldWithSubscribers : RuleDecorator
    {
        public HasFieldWithSubscribers(IRule rule) : base(rule) { }
        public HasFieldWithSubscribers(IRule rule, string targetType) : base(rule, targetType) { }

        /// <summary>
        /// Analyzes the fields in a given NamespaceDeclarationSyntax and identifies the subscribers.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax context)
        {
            // Gets the fields and identifiers and checks if they are interfaces.
            List<FieldDeclarationSyntax> fieldList = SyntaxManager.GetFieldSyntax(context);
            List<string> fieldIdentifiers = SyntaxManager.GetFieldIdentifiers(fieldList);
            bool isInterface = IsFieldIdentifierInterface(fieldIdentifiers);
            if (isInterface)
            {
                addPattern(context);
                return PartOf.Analyze(context);
            }

            return null;
        }

        /// <summary>
        /// The method checks if given string is an interface within the solution and returns a bool.
        /// </summary>
        /// <param name="identifiers"></param>
        /// <returns></returns>
        public bool IsFieldIdentifierInterface(List<string> identifiers)
        {
            List<string> interfaces = new List<string>();
            bool isInterface = false;

            try
            {
                var model = this.StoredSemanticModel.Compilation.SyntaxTrees;

                foreach (var file in model)
                {
                    // Get a SyntaxNode of the file to browse through.
                    var tree = (CompilationUnitSyntax)file.GetRoot();

                    // Create an iterable of the tree
                    var children = tree.DescendantNodes();

                    // Iterate through the file's nodes to find a NamespaceDeclarationSyntax
                    foreach (var child in children)
                    {
                        if (child.GetType() == typeof(NamespaceDeclarationSyntax))
                        {
                            // Een namespacedeclarationsyntax gevonden :o
                            var parsedChild = (NamespaceDeclarationSyntax)child;

                            // Implemented interfaces
                            interfaces = SyntaxManager.GetInterface(parsedChild);
                        }
                    }
                }
            }
            catch (Exception e)
            {

                var error = e.Message;
            }

            // Checks if the identifier is an interface and sets 'isInterface' to true if it is an interface.
            try
            {
                foreach (var identifier in identifiers)
                {
                    foreach (var item in interfaces)
                    {
                        if (item == identifier)
                        {
                            isInterface = true;
                        }
                    }
                }
            }
            catch
            {
                throw new RuleException("Interfaces not found.");
            }


            return isInterface;
        }

        /// <summary>
        /// Gets the file locations of the field identifiers if they are an interface.
        /// </summary>
        /// <param name="identifiers"></param>
        /// <returns></returns>
        public Hashtable GetFileLocation(List<string> identifiers)
        {
            Hashtable fileLocations = new Hashtable();

            try
            {
                var model = this.StoredSemanticModel.Compilation.SyntaxTrees;

                foreach (var file in model)
                {
                    // Get a SyntaxNode of the file to browse through.
                    var tree = (CompilationUnitSyntax)file.GetRoot();

                    // Create an iterable of the tree
                    var children = tree.DescendantNodes();

                    // Iterate through the file's nodes to find a NamespaceDeclarationSyntax
                    foreach (var child in children)
                    {
                        if (child.GetType() == typeof(NamespaceDeclarationSyntax))
                        {
                            // Een namespacedeclarationsyntax gevonden :o
                            var parsedChild = (NamespaceDeclarationSyntax)child;
                            var filePath = parsedChild.SyntaxTree.FilePath;

                            // Implemented interfaces
                            var interfaces = SyntaxManager.GetInterface(parsedChild);

                            // Adds the location of the class file to 'fileLocations' of the class that implements the found interfaces.
                            if (interfaces != null)
                            {
                                foreach (var item in interfaces)
                                {
                                    fileLocations.Add(item, filePath);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

                var error = e.Message;
            }

            return fileLocations;
        }



    }

}
