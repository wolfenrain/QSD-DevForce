﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule
{
    class IsConcreteProduct : RuleDecorator
    {
        public IsConcreteProduct(IRule rule) : base(rule) { }
        public IsConcreteProduct(IRule rule, string targetType) : base(rule, targetType) { }

        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax context)
        {
            /// Check if HayStack to search for is defined and throw an error if not.
            /// Check if the classlist is not empty
            if (this.HayStack == null || (this.Classes == null || this.Classes.Count == 0))
            {
                throw new RuleException("No Rule.HayStack defined for searching implementations");
            }


            bool minimumMatch = false;
            NamespaceDeclarationSyntax namespaceFound = null;

            // Loop through all eligible files.
            foreach (var classreference in this.Classes)
            {
                // Find the file in the syntax tree.
                var model = this.StoredSemanticModel.Compilation.SyntaxTrees;
                foreach (var file in model)
                {
                    if (file.FilePath.Equals(classreference.Path))
                    {
                        // File found. Parse to tree
                        var tree = (CompilationUnitSyntax)file.GetRoot();
                        var children = tree.DescendantNodes();
                        foreach (var child in children)
                        {
                            if (child.GetType() == typeof(NamespaceDeclarationSyntax))
                            {
                                namespaceFound = (NamespaceDeclarationSyntax)child;
                            }

                            if (child.GetType() == typeof(ClassDeclarationSyntax))
                            {
                                var classdeclaration = (ClassDeclarationSyntax)child;
                                foreach (var importedClass in classdeclaration.BaseList?.ChildNodes())
                                {
                                    var target = importedClass.GetText().ToString();
                                    target = target.Replace("\n", string.Empty);
                                    target = target.Replace("\r", string.Empty);

                                    if (target.Equals(this.Needle))
                                    {
                                        minimumMatch = true;

                                        addPattern(namespaceFound);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (minimumMatch)
            {
                return this.PartOf.Analyze(context);
            }

            throw new RuleException("No matches found in classlist.");
        }
    }
}
