﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;

namespace QSD_DevForce.Analyzer.Rule
{
    public class HasPrivateStaticField : RuleDecorator
    {

        public HasPrivateStaticField(IRule rule) : base(rule) { }

        public HasPrivateStaticField(IRule rule, string targetType) : base(rule, targetType) { }

        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax node)
        {
            //get the singleton field
            FieldDeclarationSyntax obj = SyntaxManager.GetSingletonFieldSyntax(node);

            if (obj != null)
            {
                var fieldType = obj.Declaration.Type.ToString();
                var className = SyntaxManager.GetClassName(node);
                var isPrivate = false;
                var isStatic = false;

                //Checks if a field/attribute is private and static. If both are true it passes the node to the next rule.
                if (className.Equals(fieldType))
                {
                    foreach (var item in obj.Modifiers)
                    {
                        if (item.ValueText == "private")
                        {
                            isPrivate = true;
                        }
                        else if (item.ValueText == "static")
                        {
                            isStatic = true;
                        }
                    }
                    if (isPrivate && isStatic)
                    {
                        addPattern(node);
                        return PartOf.Analyze(node);
                    }
                }
            }

            throw new RuleException("No private static singleton field found");
        }
    }
}
