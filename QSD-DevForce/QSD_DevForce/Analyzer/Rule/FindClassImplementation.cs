﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule
{
    class FindClassImplementation : RuleDecorator
    {
        public FindClassImplementation(IRule rule) : base(rule) { }
        public FindClassImplementation(IRule rule, string targetType) : base(rule, targetType) { }

        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax context)
        {
            /// Check if HayStack to search for is defined and throw an error if not.
            if (this.PartOf.HayStack == null)
            {
                throw new RuleException("No Rule.HayStack defined for searching implementations");
            }

            bool minimumMatch = false;

            /// Use the semantic model to find implementations of the interface.
            try
            {
                var model = this.StoredSemanticModel.Compilation.SyntaxTrees;

                foreach (var file in model)
                {
                    // Get a SyntaxNode of the file to browse through.
                    var tree = (CompilationUnitSyntax)file.GetRoot();

                    // Create an iterable of the tree
                    var children = tree.DescendantNodes();

                    // Iterate through the file's nodes to find a NamespaceDeclarationSyntax
                    foreach (var child in children)
                    {
                        if (child.GetType() == typeof(NamespaceDeclarationSyntax))
                        {
                            // If a NameSpaceDeclarationSyntx is found, parse it to a child.
                            var parsedChild = (NamespaceDeclarationSyntax)child;

                            // Implemented interfaces
                            var interfaces = SyntaxManager.GetInterface(parsedChild);

                            // Add interface items to LinkedList
                            if (interfaces != null)
                            {
                                foreach (var item in interfaces)
                                {
                                    if (item.Equals(this.HayStack))
                                    {
                                        minimumMatch = true;
                                        addPattern(parsedChild);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                throw new RuleException("Semantic tree has not been initialized yet.");
            }

            if (!minimumMatch)
            {
                throw new RuleException("No concrete creators found for class.");
            }

            return PartOf.Analyze(context);
        }
    }
}
