﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;

namespace QSD_DevForce.Analyzer.Rule
{
    public class HasPrivateConstructor : RuleDecorator
    {
        public HasPrivateConstructor(IRule rule) : base(rule) { }

        public HasPrivateConstructor(IRule rule, string targetType) : base(rule, targetType) { }

        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax node)
        {
            //get the constructor node
            ConstructorDeclarationSyntax constructorNode = SyntaxManager.GetConstructorSyntax(node);

            //Checks if the constructor is private.
            if (constructorNode != null)
            {
                foreach (var item in constructorNode.Modifiers)
                {
                    if (item.ValueText == "private")
                    {
                        addPattern(node);
                        return PartOf.Analyze(node);
                    }
                }
            }
            throw new RuleException("no private constructor found");
        }
    }
}
