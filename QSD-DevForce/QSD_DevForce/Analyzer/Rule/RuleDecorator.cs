﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule
{
    public abstract class RuleDecorator : IRule
    {
        public IRule PartOf;

        public string TypeName
        {
            get { return PartOf.TypeName; }
            set { PartOf.TypeName = value; }
        }

        public string HayStack
        {
            get { return PartOf.HayStack; }
            set { PartOf.HayStack = value; }
        }

        public string Needle
        {
            get { return PartOf.Needle; }
            set { PartOf.Needle = value; }
        }

        public SemanticModel StoredSemanticModel
        {
            get { return PartOf.StoredSemanticModel; }
            set { PartOf.StoredSemanticModel = value; }
        }

        public List<ClassReference> Classes
        {
            get { return PartOf.Classes; }
            set { PartOf.Classes = value; }
        }

        public List<Pattern> Patterns
        {
            get { return PartOf.Patterns; }
            set { PartOf.Patterns = value; }
        }

        public string PatternGroupIdentifier
        {
            get { return PartOf.PatternGroupIdentifier; }
            set { PartOf.PatternGroupIdentifier = value; }
        }

        public string TargetType { get; set; }

        public RuleDecorator(IRule partOf)
        {
            PartOf = partOf;
        }

        public RuleDecorator(IRule partOf, string targetType) : this(partOf)
        {
            TargetType = targetType;
        }

        public abstract NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax context);

        public void addPattern(NamespaceDeclarationSyntax context)
        {
            if (this.TargetType == null)
            {
                return;
            }
            Uri location;
            try
            {
                location = new Uri(context.SyntaxTree.FilePath);
            }
            catch
            {
                location = null;
            }
            this.Patterns.Add(new Pattern() { Title = "", TargetType = this.TargetType, Part = "", Location = location });
        }

    }
}
