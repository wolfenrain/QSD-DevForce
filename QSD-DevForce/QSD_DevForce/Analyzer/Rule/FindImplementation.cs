﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule
{
    class FindImplementation : RuleDecorator
    {
        public FindImplementation(IRule rule) : base(rule) { }
        public FindImplementation(IRule rule, string targetType) : base(rule, targetType) { }

        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax context)
        {
            /// Check if HayStack to search for is defined and throw an error if not.
            if (this.PartOf.HayStack == null)
            {
                throw new RuleException("No Rule.HayStack defined for searching implementations");
            }

            /// Use the semantic model to find implementations of the interface.
            try
            {
                var model = this.StoredSemanticModel.Compilation.SyntaxTrees;

                // Clear the strored classlist.
                this.Classes = new List<ClassReference>();

                foreach (var file in model)
                {
                    // Get a SyntaxNode of the file to browse through.
                    var tree = (CompilationUnitSyntax)file.GetRoot();

                    // Create an iterable of the tree
                    var children = tree.DescendantNodes();

                    // Iterate through the file's nodes to find a NamespaceDeclarationSyntax
                    foreach (var child in children)
                    {
                        if (child.GetType() == typeof(NamespaceDeclarationSyntax))
                        {
                            // If a NameSpaceDeclarationSyntx is found, parse it to a child.
                            var parsedChild = (NamespaceDeclarationSyntax)child;

                            // Implemented interfaces
                            var interfaces = SyntaxManager.GetInterface(parsedChild);

                            // Add interface items to LinkedList
                            if (interfaces != null)
                            {
                                foreach (var item in interfaces)
                                {
                                    if (item.Equals(this.HayStack))
                                    {
                                        this.Classes.Add(new ClassReference(item, file.FilePath));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                throw new RuleException("Semantic tree has not been initialized yet.");
            }

            // Verify if the classes list is empty, if so then throw an exception
            if (this.Classes.Count == 0)
            {
                throw new RuleException("There are no references to the interface.");
            }
            addPattern(context);

            return PartOf.Analyze(context);
        }
    }
}
