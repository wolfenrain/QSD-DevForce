﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;

namespace QSD_DevForce.Analyzer.Rule
{
    public interface IRule
    {
        NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax node);
        string TypeName { get; set; }
        string HayStack { get; set; }
        string Needle { get; set; }
        List<ClassReference> Classes { get; set; }
        List<Pattern> Patterns { get; set; }
        string PatternGroupIdentifier { get; set; }

        SemanticModel StoredSemanticModel { get; set; }
    }
}