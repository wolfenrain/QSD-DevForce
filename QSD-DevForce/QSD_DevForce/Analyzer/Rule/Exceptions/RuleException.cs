﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule.Exceptions
{
    class RuleException : Exception
    {
        public RuleException(string message) : base(message) { }

        public RuleException(string message, Exception inner) : base(message, inner) { }
    }
}
