﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule
{
    public class HasMethodReturnsType : RuleDecorator
    {
        public HasMethodReturnsType(IRule rule) : base(rule) { }
        public HasMethodReturnsType(IRule rule, string targetType) : base(rule, targetType) { }

        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax context)
        {
            if (this.HayStack == null || this.Needle == null)
            {
                throw new RuleException("Required parameters are not set for HasMethodReturnsType");
            }

            var methods = SyntaxManager.GetMethodSyntax(context);

            foreach (var method in methods)
            {
                if (method.ReturnType.ToString().Equals(this.Needle))
                {
                    addPattern(context);

                    this.PartOf.HayStack = SyntaxManager.GetClassName(context);

                    return this.PartOf.Analyze(context);
                }
            }

            throw new RuleException("No abstract factory method found.");
        }
    }
}
