﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule
{
    public class IsInterface : RuleDecorator
    {
        public IsInterface(IRule rule) : base(rule) { }
        public IsInterface(IRule rule, string targetType) : base(rule, targetType) { }

        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax context)
        {
            /// Attempt to parse first child of context to an interface declaration.
            /// Failing to will throw an error, stopping the analysis (by design).
            InterfaceDeclarationSyntax interfaceDeclaration;
            try
            {
                interfaceDeclaration = (InterfaceDeclarationSyntax)context.Members.FirstOrDefault();
            }
            catch
            {
                throw new RuleException("Member could not be parsed to an interface declaration.");
            }

            addPattern(context);

            /// Stores the interface name (Object Type name) in the rule object for later reference.
            this.PartOf.HayStack = interfaceDeclaration.Identifier.ValueText;

            /// Set groupname for patterns.
            this.PatternGroupIdentifier = this.PartOf.HayStack;

            /// Recursive call to decorated parent on same context.
            return PartOf.Analyze(context);
        }
    }
}
