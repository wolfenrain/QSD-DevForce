﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using QSD_DevForce.Analyzer.Rule.Exceptions;
using QSD_DevForce.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule
{
    class FindAbstractClassInClasslist : RuleDecorator
    {
        public FindAbstractClassInClasslist(IRule rule) : base(rule) { }
        public FindAbstractClassInClasslist(IRule rule, string targetType) : base(rule, targetType) { }

        public override NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax context)
        {
            /// Check if HayStack to search for is defined and throw an error if not.
            /// Check if the classlist is not empty
            if (this.HayStack == null || (this.Classes == null || this.Classes.Count == 0))
            {
                throw new RuleException("No Rule.HayStack defined for searching implementations");
            }

            NamespaceDeclarationSyntax namespaceFound = null;

            // Loop through all eligible files.
            foreach (var classreference in this.Classes)
            {
                // Find the file in the syntax tree.
                var model = this.StoredSemanticModel.Compilation.SyntaxTrees;
                foreach (var file in model)
                {
                    if (file.FilePath.Equals(classreference.Path))
                    {
                        // File found. Parse to tree
                        var tree = (CompilationUnitSyntax)file.GetRoot();
                        var children = tree.DescendantNodes();
                        foreach (var child in children)
                        {
                            if (child.GetType() == typeof(NamespaceDeclarationSyntax))
                            {
                                namespaceFound = (NamespaceDeclarationSyntax)child;
                            }

                            if (child.GetType() == typeof(ClassDeclarationSyntax))
                            {
                                var classdeclaration = (ClassDeclarationSyntax)child;
                                foreach (var modifier in classdeclaration.Modifiers)
                                {
                                    if (modifier.ValueText.Equals("abstract"))
                                    {
                                        foreach (var member in classdeclaration.Members)
                                        {
                                            if (member is MethodDeclarationSyntax)
                                            {
                                                var method = (MethodDeclarationSyntax)member;
                                                if (method.ReturnType.ToString().Equals(this.HayStack))
                                                {
                                                    // Creator klasse gevonden.
                                                    // Assign the Interface to the Needle value (That's what we want to search for).
                                                    this.Needle = this.HayStack;

                                                    // Assign the found class as the Haystack because we want to search in that.
                                                    this.HayStack = classdeclaration.Identifier.ValueText;

                                                    if (namespaceFound != null)
                                                    {
                                                        this.Classes.Remove(classreference);
                                                        addPattern(namespaceFound);
                                                        return PartOf.Analyze(namespaceFound);
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // Stop loop.
                        break;
                    }
                }
            }

            throw new RuleException("No abstract class found in classlist.");
        }
    }
}
