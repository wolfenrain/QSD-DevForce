﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule
{
    public class ClassReference
    {
        public string ClassName { get; set; }
        public string Path { get; set; }

        public ClassReference(string classname, string path)
        {
            ClassName = classname;
            Path = path;
        }
    }
}
