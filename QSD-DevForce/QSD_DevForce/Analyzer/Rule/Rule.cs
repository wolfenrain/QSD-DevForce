﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Analyzer.Rule
{
    public class Rule : IRule
    {
        public string HayStack { get; set; }
        public string Needle { get; set; }
        public List<ClassReference> Classes { get; set; }
        public SemanticModel StoredSemanticModel { get; set; }
        public List<Pattern> Patterns { get; set; }
        public string TypeName { get; set; }
        public string PatternGroupIdentifier { get; set; }

        public Rule()
        {
            Patterns = new List<Pattern>();
        }

        /// <summary>
        /// Returns the node.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public NamespaceDeclarationSyntax Analyze(NamespaceDeclarationSyntax node) => node;
    }
}
