﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace QSD_DevForce.Syntax
{
    public class SyntaxManager
    {
        // This method retrieves the constructor from the namespace.
        public static ConstructorDeclarationSyntax GetConstructorSyntax(NamespaceDeclarationSyntax syntax)
        {
            // This variable retrieves the class from the namespace.
            var classDeclaration = (ClassDeclarationSyntax)syntax.Members.FirstOrDefault();

            // The foreach loop checks every item in the class and tries to find the constructor in the class.
            foreach (var item in classDeclaration.Members)
            {
                if (item is ConstructorDeclarationSyntax cons)
                {
                    return cons;
                }
            }

            return null;
        }

        // This method retrieves the field that matches the singleton pattern.
        public static FieldDeclarationSyntax GetSingletonFieldSyntax(NamespaceDeclarationSyntax syntax)
        {
            // This variable retrieves the class from the namespace.
            var classDeclaration = (ClassDeclarationSyntax)syntax.Members.FirstOrDefault();

            // The foreach loop checks every item in the class and tries to find the singleton field.
            foreach (var item in classDeclaration.Members)
            {
                if (item is FieldDeclarationSyntax prop)
                {
                    return prop;
                }
            }

            return null;
        }

        // This method retrieves all the fields from the class.
        public static List<FieldDeclarationSyntax> GetFieldSyntax(NamespaceDeclarationSyntax syntax)
        {
            // This variable retrieves the class from the namespace.
            var classDeclaration = (ClassDeclarationSyntax)syntax.Members.FirstOrDefault();
            List<FieldDeclarationSyntax> fields = new List<FieldDeclarationSyntax>();

            // The foreach loop retrieves all the fields that are in the class.
            foreach (var item in classDeclaration.Members)
            {
                if (item is FieldDeclarationSyntax prop) fields.Add(prop);
            }

            return fields;
        }

        // This method tries to find if there is a function that matches the singleton pattern.
        public static MethodDeclarationSyntax GetSingletonMethodSyntax(NamespaceDeclarationSyntax syntax)
        {
            // This variable retrieves the class from the namespace.
            var classDeclaration = (ClassDeclarationSyntax)syntax.Members.FirstOrDefault();

            // The foreach loop tries to retrieve the singleton method.
            foreach (var item in classDeclaration.Members)
            {
                if (item is MethodDeclarationSyntax method)
                {
                    return method;
                }
            }

            return null;
        }

        // This method retrieves all the methods from the class.
        public static List<MethodDeclarationSyntax> GetMethodSyntax(NamespaceDeclarationSyntax syntax)
        {
            // This variable retrieves the class from the namespace.
            var classDeclaration = (ClassDeclarationSyntax)syntax.Members.FirstOrDefault();
            List<MethodDeclarationSyntax> methods = new List<MethodDeclarationSyntax>();

            // The forach loop retrieves all the methods that are in the class.
            foreach (var item in classDeclaration.Members)
            {
                if (item is MethodDeclarationSyntax method) methods.Add(method);
            }

            return methods;
        }

        // This method retrieves all the inheritated implemented interfaces in the class.
        public static List<string> GetInterface(NamespaceDeclarationSyntax syntax)
        {
            // This variable retrieves the class from the namespace.
            ClassDeclarationSyntax classDeclaration;

            try
            {
                classDeclaration = (ClassDeclarationSyntax)syntax.Members.FirstOrDefault();
            }
            catch
            {
                // If object can't be cast to a ClassDeclarationSyntax; return an empty List.
                // @TODO: Allow for ClassDeclarationSyntax AS WELL AS FOR InterfaceDeclarationSyntax.
                return new List<string>();
            }

            var baseList = classDeclaration?.BaseList;
            List<string> interfaces = new List<string>();

            if (baseList == null)
            {
                return null;
            }

            // The foreach loop retrieves the interfaces from the baseList and returns all the interfaces it finds.
            foreach (var node in baseList.ChildNodes())
            {
                string interfaceName = node.GetText().ToString();

                interfaceName = interfaceName.Replace("\n", string.Empty);
                interfaceName = interfaceName.Replace("\r", string.Empty);

                interfaces.Add(interfaceName);
            }

            return interfaces;
        }

        // This method retrieves the class name from the class.
        public static string GetClassName(NamespaceDeclarationSyntax syntax)
        {
            // This variable retrieves the class from the namespace.
            var classDeclaration = (ClassDeclarationSyntax)syntax.Members.FirstOrDefault();
            var className = classDeclaration.Identifier.ValueText;

            return className;
        }

        public static List<string> GetFieldIdentifiers(List<FieldDeclarationSyntax> fieldList)
        {
            List<string> fieldIdentifiers = new List<string>();

            //loops through the list of fields and returns the field identifier.
            foreach (var item in fieldList)
            {
                //Null check.
                if (item == null)
                {
                    return null;
                }

                //Checks if the field is a List<T>.
                if (item.Declaration.Type is GenericNameSyntax)
                {
                    var childEnumerator = item.Declaration.Type.ChildNodes().GetEnumerator();

                    //Find what type of objects are in the List<T>
                    while (childEnumerator.MoveNext())
                    {
                        fieldIdentifiers.Add(childEnumerator.Current.GetText().ToString().Trim('<').Trim(' ').Trim('>'));
                    }
                }

                //Checks if the field is an array.
                if (item.Declaration.Type is ArrayTypeSyntax)
                {
                    var childEnumerator = item.Declaration.Type.ChildNodes().GetEnumerator();

                    //Find what type of objects are in the array.
                    while (childEnumerator.MoveNext())
                    {
                        fieldIdentifiers.Add(childEnumerator.Current.Parent.GetText().ToString().Trim(' ').TrimEnd(']').TrimEnd('['));
                    }
                }

                //Checks if the field is not an array or List<T>.
                if (!(item.Declaration.Type is ArrayTypeSyntax || item.Declaration.Type is GenericNameSyntax))
                {
                    fieldIdentifiers.Add(item.Declaration.Type.ToString());
                }
            }

            return fieldIdentifiers;
        }
    }
}
