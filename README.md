# QSD Devforce plugin app voor VS2020

## Definition of done

De definition of done geld over het hele traject van het issue, niet alleen de oplevering. De volgorde van het uitwerken en controleren van deze punten is dus van belang!

- De oplossing is beschreven in het Functioneel Ontwerp;
  - Use Case is toegevoegd aan het use-case diagram;
  - Use Case beschrijving is toegevoegd;
  - Schermontwerp ingevoegd wanneer van toepassing;
- De oplossing is inhoudelijk gecontroleerd door een reviewer;
- De oplossing is netjes en conform conventie;
- De oplossing produceert geen foutmeldingen;
- De oplossing is testbaar gemaakt door middel van unit-tests;
- De oplossing is gedocumenteerd in het Technisch Ontwerp;
- De oplossing is succesvol gemerged naar master en heeft het CI/CD proces doorstaan.

---
